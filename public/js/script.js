// ---------Responsive-navbar-active-animation-----------
function test() {
    var tabsNewAnim = $('#navbarSupportedContent');
    var selectorNewAnim = $('#navbarSupportedContent').find('li').length;
    var activeItemNewAnim = tabsNewAnim.find('.active');
    var activeWidthNewAnimHeight = activeItemNewAnim.innerHeight();
    var activeWidthNewAnimWidth = activeItemNewAnim.innerWidth();
    var itemPosNewAnimTop = activeItemNewAnim.position();
    var itemPosNewAnimLeft = activeItemNewAnim.position();
    $(".hori-selector").css({
        "top": itemPosNewAnimTop.top + "px",
        "left": itemPosNewAnimLeft.left + "px",
        "height": activeWidthNewAnimHeight + "px",
        "width": activeWidthNewAnimWidth + "px"
    });
    $("#navbarSupportedContent").on("click", "li", function (e) {
        $('#navbarSupportedContent ul li').removeClass("active");
        $(this).addClass('active');
        var activeWidthNewAnimHeight = $(this).innerHeight();
        var activeWidthNewAnimWidth = $(this).innerWidth();
        var itemPosNewAnimTop = $(this).position();
        var itemPosNewAnimLeft = $(this).position();
        $(".hori-selector").css({
            "top": itemPosNewAnimTop.top + "px",
            "left": itemPosNewAnimLeft.left + "px",
            "height": activeWidthNewAnimHeight + "px",
            "width": activeWidthNewAnimWidth + "px"
        });
    });
}
$(document).ready(function () {

    // navbar 

    setTimeout(function () {
        test();

        $('.home').click(() => {
            $('.category').css('display', 'none');
        });

        $('.btn-category').click(() => {
            $('.category').css('display', 'block');
        });

        $('.close-category').click(() => {
            $('.category').css('display', 'none');
        });

        $('.about').click(() => {
            $('.category').css('display', 'none');

            $.get('/api/v1/adib_cs/:adib_cs/blog/:blog/commits', (data, status) => {
                console.log(data + '  dan ' + status);
            });

            //   $('a[href*=\\#]').on('click', function(event){     // biar link menuju idnya pas nyecroll smooth
            //     event.preventDefault();
            //     $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
            // });
        });
    });

    // header 

   
 

});
$(window).on('resize', function () {
    setTimeout(function () {
        test();
    }, 500);
});
$(".navbar-toggler").click(function () {
    setTimeout(function () {
        test();
    });
});

// header 

 document.addEventListener('DOMContentLoaded', function() {
        var parent = document.querySelector('.splitview'),
            topPanel = parent.querySelector('.top'),
            handle = parent.querySelector('.handle'),
            skewHack = 0,
            delta = 0;

        // If the parent has .skewed class, set the skewHack var.
        if (parent.className.indexOf('skewed') != -1) {
            skewHack = 1000;
        }

        parent.addEventListener('mousemove', function(event) {
            // Get the delta between the mouse position and center point.
            delta = (event.clientX - window.innerWidth / 2) * 0.1;

            // Move the handle.
            delta1 = handle.style.left = event.clientX + (delta + 300) + 'px';

            // console.log(delta1);
            

            // Adjust the top panel width.
            topPanel.style.width = event.clientX + skewHack + (delta + 300) + 'px';
        });
    });

    // content header 

    $(document).ready(function () {
        var walkthrough;
        walkthrough = {
          index: 0,
          nextScreen: function () {
            if (this.index < this.indexMax()) {
              this.index++;
              return this.updateScreen();
            }
          },
          prevScreen: function () {
            if (this.index > 0) {
              this.index--;
              return this.updateScreen();
            }
          },
          updateScreen: function () {
            this.reset();
            this.goTo(this.index);
            return this.setBtns();
          },
          setBtns: function () {
            var $lastBtn, $nextBtn, $prevBtn;
            $nextBtn = $('.next-screen');
            $prevBtn = $('.prev-screen');
            $lastBtn = $('.finish');
            if (walkthrough.index === walkthrough.indexMax()) {
              $nextBtn.prop('disabled', true);
              $prevBtn.prop('disabled', false);
              return $lastBtn.addClass('active').prop('disabled', false);
            } else if (walkthrough.index === 0) {
              $nextBtn.prop('disabled', false);
              $prevBtn.prop('disabled', true);
              return $lastBtn.removeClass('active').prop('disabled', true);
            } else {
              $nextBtn.prop('disabled', false);
              $prevBtn.prop('disabled', false);
              return $lastBtn.removeClass('active').prop('disabled', true);
            }
          },
          goTo: function (index) {
            $('.screen').eq(index).addClass('active');
            return $('.dot').eq(index).addClass('active');
          },
          reset: function () {
            return $('.screen, .dot').removeClass('active');
          },
          indexMax: function () {
            return $('.screen').length - 1;
          },
          closeModal: function () {
            $('.walkthrough, .shade').removeClass('reveal');
            return setTimeout(() => {
              $('.walkthrough, .shade').removeClass('show');
              this.index = 0;
              return this.updateScreen();
            }, 200);
          },
          openModal: function () {
            $('.walkthrough, .shade').addClass('show');
            setTimeout(() => {
              return $('.walkthrough, .shade').addClass('reveal');
            }, 200);
            return this.updateScreen();
          } };
    
        $('.next-screen').click(function () {
          return walkthrough.nextScreen();
        });
        $('.prev-screen').click(function () {
          return walkthrough.prevScreen();
        });
        $('.open-walkthrough').click(function () {
          return walkthrough.openModal();
        });
        walkthrough.openModal();
    
        // Optionally use arrow keys to navigate walkthrough
        return $(document).keydown(function (e) {
          switch (e.which) {
            case 37:
              // left
              walkthrough.prevScreen();
              break;
            case 38:
              // up
              walkthrough.openModal();
              break;
            case 39:
              // right
              walkthrough.nextScreen();
              break;
            case 40:
              // down
              walkthrough.closeModal();
              break;
            default:
              return;}
    
          e.preventDefault();
        });


        
        
    });
    

    //   content 2 
    
    $( () => {
        var walkthrough_1;
        walkthrough_1 = {
          index: 0,
          nextScreen_1: function () {
            if (this.index < this.indexMax()) {
              this.index++;
              return this.updateScreen_1();
            }
          },
          prevScreen_1: function () {
            if (this.index > 0) {
              this.index--;
              return this.updateScreen_1();
            }
          },
          updateScreen_1: function () {
            this.reset();
            this.goTo(this.index);
            return this.setBtns();
          },
          setBtns: function () {
            var $lastBtn, $nextBtn_1, $prevBtn_1;
            $nextBtn_1 = $('.next-screen_1');
            $prevBtn_1 = $('.prev-screen_1');
            $lastBtn = $('.finish');
            if (walkthrough_1.index === walkthrough_1.indexMax()) {
              $nextBtn_1.prop('disabled', true);
              $prevBtn_1.prop('disabled', false);
              return $lastBtn.addClass('active_1').prop('disabled', false);
            } else if (walkthrough_1.index === 0) {
              $nextBtn_1.prop('disabled', false);
              $prevBtn_1.prop('disabled', true);
              return $lastBtn.removeClass('active_1').prop('disabled', true);
            } else {
              $nextBtn_1.prop('disabled', false);
              $prevBtn_1.prop('disabled', false);
              return $lastBtn.removeClass('active_1').prop('disabled', true);
            }
          },
          goTo: function (index) {
            $('.screen_1').eq(index).addClass('active_1');
            return $('.dot_1').eq(index).addClass('active_1');
          },
          reset: function () {
            return $('.screen_1, .dot_1').removeClass('active_1');
          },
          indexMax: function () {
            return $('.screen_1').length - 1;
          },
          closeModal: function () {
            $('.walkthrough_1, .shade_1').removeClass('reveal_1');
            return setTimeout(() => {
              $('.walkthrough_1, .shade_1').removeClass('show_1');
              this.index = 0;
              return this.updateScreen_1();
            }, 200);
          },
          openModal: function () {
            $('.walkthrough_1, .shade_1').addClass('show_1');
            setTimeout(() => {
              return $('.walkthrough_1, .shade_1').addClass('reveal_1');
            }, 200);
            return this.updateScreen_1();
          } };
    
        $('.next-screen_1').click(function () {
          return walkthrough_1.nextScreen_1();
        });
        $('.prev-screen_1').click(function () {
          return walkthrough_1.prevScreen_1();
        });
        $('.open-walkthrough_1').click(function () {
          return walkthrough_1.openModal();
        });
        walkthrough_1.openModal();
    
        // Optionally use arrow keys to navigate walkthrough_1
        return $(document).keydown(function (e) {
          switch (e.which) {
            case 37:
              // left
              walkthrough_1.prevScreen_1();
              break;
            case 38:
              // up
              walkthrough_1.openModal();
              break;
            case 39:
              // right
              walkthrough_1.nextScreen_1();
              break;
            case 40:
              // down
              walkthrough_1.closeModal();
              break;
            default:
              return;}
    
          e.preventDefault();
        });
      })


      //   content 3 
    
    $( () => {
      var walkthrough_2;
      walkthrough_2 = {
        index: 0,
        nextScreen_2: function () {
          if (this.index < this.indexMax()) {
            this.index++;
            return this.updateScreen_2();
          }
        },
        prevScreen_2: function () {
          if (this.index > 0) {
            this.index--;
            return this.updateScreen_2();
          }
        },
        updateScreen_2: function () {
          this.reset();
          this.goTo(this.index);
          return this.setBtns();
        },
        setBtns: function () {
          var $lastBtn, $nextBtn_2, $prevBtn_2;
          $nextBtn_2 = $('.next-screen_2');
          $prevBtn_2 = $('.prev-screen_2');
          $lastBtn = $('.finish');
          if (walkthrough_2.index === walkthrough_2.indexMax()) {
            $nextBtn_2.prop('disabled', true);
            $prevBtn_2.prop('disabled', false);
            return $lastBtn.addClass('active_2').prop('disabled', false);
          } else if (walkthrough_2.index === 0) {
            $nextBtn_2.prop('disabled', false);
            $prevBtn_2.prop('disabled', true);
            return $lastBtn.removeClass('active_2').prop('disabled', true);
          } else {
            $nextBtn_2.prop('disabled', false);
            $prevBtn_2.prop('disabled', false);
            return $lastBtn.removeClass('active_2').prop('disabled', true);
          }
        },
        goTo: function (index) {
          $('.screen_2').eq(index).addClass('active_2');
          return $('.dot_2').eq(index).addClass('active_2');
        },
        reset: function () {
          return $('.screen_2, .dot_2').removeClass('active_2');
        },
        indexMax: function () {
          return $('.screen_2').length - 1;
        },
        closeModal: function () {
          $('.walkthrough_2, .shade_2').removeClass('reveal_2');
          return setTimeout(() => {
            $('.walkthrough_2, .shade_2').removeClass('show_2');
            this.index = 0;
            return this.updateScreen_2();
          }, 200);
        },
        openModal: function () {
          $('.walkthrough_2, .shade_2').addClass('show_2');
          setTimeout(() => {
            return $('.walkthrough_2, .shade_2').addClass('reveal_2');
          }, 200);
          return this.updateScreen_2();
        } };
  
      $('.next-screen_2').click(function () {
        return walkthrough_2.nextScreen_2();
      });
      $('.prev-screen_2').click(function () {
        return walkthrough_2.prevScreen_2();
      });
      $('.open-walkthrough_2').click(function () {
        return walkthrough_2.openModal();
      });
      walkthrough_2.openModal();
  
      // Optionally use arrow keys to navigate walkthrough_2
      return $(document).keydown(function (e) {
        switch (e.which) {
          case 37:
            // left
            walkthrough_2.prevScreen_2();
            break;
          case 38:
            // up
            walkthrough_2.openModal();
            break;
          case 39:
            // right
            walkthrough_2.nextScreen_2();
            break;
          case 40:
            // down
            walkthrough_2.closeModal();
            break;
          default:
            return;}
  
        e.preventDefault();
      });
    })



      //   content 4 
    
      $( () => {
        var walkthrough_3;
        walkthrough_3 = {
          index: 0,
          nextScreen_3: function () {
            if (this.index < this.indexMax()) {
              this.index++;
              return this.updateScreen_3();
            }
          },
          prevScreen_3: function () {
            if (this.index > 0) {
              this.index--;
              return this.updateScreen_3();
            }
          },
          updateScreen_3: function () {
            this.reset();
            this.goTo(this.index);
            return this.setBtns();
          },
          setBtns: function () {
            var $lastBtn, $nextBtn_3, $prevBtn_3;
            $nextBtn_3 = $('.next-screen_3');
            $prevBtn_3 = $('.prev-screen_3');
            $lastBtn = $('.finish');
            if (walkthrough_3.index === walkthrough_3.indexMax()) {
              $nextBtn_3.prop('disabled', true);
              $prevBtn_3.prop('disabled', false);
              return $lastBtn.addClass('active_3').prop('disabled', false);
            } else if (walkthrough_3.index === 0) {
              $nextBtn_3.prop('disabled', false);
              $prevBtn_3.prop('disabled', true);
              return $lastBtn.removeClass('active_3').prop('disabled', true);
            } else {
              $nextBtn_3.prop('disabled', false);
              $prevBtn_3.prop('disabled', false);
              return $lastBtn.removeClass('active_3').prop('disabled', true);
            }
          },
          goTo: function (index) {
            $('.screen_3').eq(index).addClass('active_3');
            return $('.dot_3').eq(index).addClass('active_3');
          },
          reset: function () {
            return $('.screen_3, .dot_3').removeClass('active_3');
          },
          indexMax: function () {
            return $('.screen_3').length - 1;
          },
          closeModal: function () {
            $('.walkthrough_3, .shade_3').removeClass('reveal_3');
            return setTimeout(() => {
              $('.walkthrough_3, .shade_3').removeClass('show_3');
              this.index = 0;
              return this.updateScreen_3();
            }, 200);
          },
          openModal: function () {
            $('.walkthrough_3, .shade_3').addClass('show_3');
            setTimeout(() => {
              return $('.walkthrough_3, .shade_3').addClass('reveal_3');
            }, 200);
            return this.updateScreen_3();
          } };
    
        $('.next-screen_3').click(function () {
          return walkthrough_3.nextScreen_3();
        });
        $('.prev-screen_3').click(function () {
          return walkthrough_3.prevScreen_3();
        });
        $('.open-walkthrough_3').click(function () {
          return walkthrough_3.openModal();
        });
        walkthrough_3.openModal();
    
        // Optionally use arrow keys to navigate walkthrough_3
        return $(document).keydown(function (e) {
          switch (e.which) {
            case 37:
              // left
              walkthrough_3.prevScreen_3();
              break;
            case 38:
              // up
              walkthrough_3.openModal();
              break;
            case 39:
              // right
              walkthrough_3.nextScreen_3();
              break;
            case 40:
              // down
              walkthrough_3.closeModal();
              break;
            default:
              return;}
    
          e.preventDefault();
        });
      })