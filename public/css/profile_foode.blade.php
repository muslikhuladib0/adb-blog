@extends('layouts.foode')

@section('title', 'profile')

@section('head')
    	<!-- Font -->
	
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700%7CAllura" rel="stylesheet">
	
	<!-- Stylesheets -->
	
	<link href="common-css/ionicons.css" rel="stylesheet">
	
	<link href="common-css/fluidbox.min.css" rel="stylesheet">
	
	<link href="01-cv-portfolio/css/style2.css" rel="stylesheet">
	
    <link href="01-cv-portfolio/css/responsive.css" rel="stylesheet">
    
@endsection

@section('content')
    <section class="intro-section">
        <div class="container">
            <div class="row">
                <div class="col-md-1 col-lg-2"></div>
                <div class="col-md-10 col-lg-8">
                    <div class="intro">
                        <div class="profile-img"><img src="images/profile-1-250x250.jpg" alt=""></div>
                        <h2><b>Michel SMith</b></h2>
                        <h4 class="font-yellow">Key Account Manager</h4>
                        <ul class="information margin-tb-30">
                            <li><b>BORN : </b>August 25, 1987</li>
                            <li><b>EMAIL : </b>mymith@mywebpage.com</li>
                            <li><b>MARITAL STATUS : </b>Married</li>
                        </ul>
                        <ul class="social-icons">
                            <li><a href="#"><i class="ion-social-pinterest"></i></a></li>
                            <li><a href="#"><i class="ion-social-linkedin"></i></a></li>
                            <li><a href="#"><i class="ion-social-instagram"></i></a></li>
                            <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                        </ul>
                    </div><!-- intro -->
                </div><!-- col-sm-8 -->
            </div><!-- row -->
        </div><!-- container -->
    </section>
@endsection