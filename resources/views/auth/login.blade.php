@extends('layouts.foode')

@section('html')
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endsection

@section('head')
    <link rel="stylesheet" href="{{ asset('css/style2.css') }}">  
    {{-- <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> --}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">   --}}
@endsection

{{-- @if(session()->get('errors'))
{{ dd($errors->getBag('default')->first()) }}
@endif --}}

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

    <br><br><br><br><br><br>
    @if ($loginOrRegister == 'login')
        <form method="POST" action="{{ route('login') }}" class="text-center">
            @csrf
            <div class="form" id="form">
                <div class="field email">
                    <div class="icon"><i class="fas fa-at"></i></div>
                    <input id="email" placeholder="masukan email terdaftar" class="input" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" required autocomplete="email">
                </div>
                <div class="field password">
                    <div class="icon"><i class="fas fa-key"></i></div>
                    <input id="password" placeholder="masukan password" class="input" type="password"
                        class="form-control @error('password') is-invalid @enderror" name="password" required
                        autocomplete="current-password">
                    </div>
                <button class="button" id="submit">LOGIN
                    <div class="side-top-bottom"></div>
                    <div class="side-left-right"></div>
                </button>
            </div>
            <div class="container mt-5 mb-4 text-center">
                @if (Route::has('password.request'))
                <a class="txt-gradient mr-2" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a> |
                @endif
                <a href="/user/register" class="txt-gradient ml-2">Create Account</a>
            </div>
            @error('email')
                <span class="error">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </form>        
    @else
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form" id="form">
            <div class="field username">
                <div class="icon"><i class="far fa-user-circle"></i></div>
                {{-- <input class="input" id="email" type="email" placeholder="Email" autocomplete="off" /> --}}
                <input id="email" placeholder="masukan username" class="input" type="text" class="@error('username') is-invalid @enderror" name="username" value="{{ old('username') }}"  autocomplete="username">

                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="field email">
                <div class="icon"><i class="fas fa-at"></i></div>
                {{-- <input class="input" id="email" type="email" placeholder="Email" autocomplete="off" /> --}}
                <input id="email" placeholder="masukan email" class="input" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">

                @error('email') 
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="field password">
                <div class="icon"><i class="fas fa-lock-open"></i></div>
                {{-- <input class="input" id="password" type="password" placeholder="Password" /> --}}

                <input id="password" placeholder="masukan password" class="input" type="password" class="@error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="field password">
                <div class="icon"><i class="fas fa-lock"></i></div>
                <input id="password-confirm" placeholder="masukan password yang sama diatas" type="password" class="input" class="form-control" name="password_confirmation"  autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <button type="submit" class="button" id="submit">
                <div class="side-top-bottom"></div>
                <div class="side-left-right"></div>
                {{ __('Register') }}
            </button>
        </div>
    </form>
        <div class="container mt-5 mb-4 text-center">
            <a href="/user/login" class="txt-gradient ml-2" style="font-size: 1.5em;">Login</a>
        </div>

    @endif

    <br> 





    <br><br><br><br><br><br>
@endsection

@push('script')
    <script>
        var formAnim = {
            $form: $('#form'),
            animClasses: ['face-up-left', 'face-up-right', 'face-down-left', 'face-down-right', 'form-complete', 'form-error'],
            resetClasses: function () {
                var $form = this.$form;

                $.each(this.animClasses, function (k, c) {
                    $form.removeClass(c);
                });
            },
            faceDirection: function (d) {
                this.resetClasses();

                d = parseInt(d) < this.animClasses.length ? d : -1;

                if (d >= 0) {
                    this.$form.addClass(this.animClasses[d]);
                }
            }
        }

        var $input = $('#email, #password'),
            $submit = $('#submit'),
            focused = false,
            completed = false;


        $input.focus(function () {
            focused = true;

            if (completed) {
                formAnim.faceDirection(1);
            } else {
                formAnim.faceDirection(0);
            }
        });

        $input.blur(function () {
            formAnim.resetClasses();
        });

        $input.on('input paste keyup', function () {
            completed = true;

            $input.each(function () {
                if (this.value == '') {
                    completed = false;
                }
            });

            if (completed) {
                formAnim.faceDirection(1);
            } else {
                formAnim.faceDirection(0);
            }
        });

        $submit.click(function () {
            focused = true;
            formAnim.resetClasses();

            if (completed) {
                $submit.css('pointer-events', 'none');
                setTimeout(function () {
                    formAnim.faceDirection(4);
                    $input.val('');
                    completed = false;

                    setTimeout(function () {
                        $submit.css('pointer-events', '');
                        formAnim.resetClasses();
                    }, 2000);
                }, 1000);
            } else {
                setTimeout(function () {
                    formAnim.faceDirection(5);

                    setTimeout(function () {
                        formAnim.resetClasses();
                    }, 2000);
                }, 1000);
            }
        });

        $(function () {
            setTimeout(function () {
                if (!focused) {
                    $input.eq(0).focus();
                }
            }, 2000);
        })
    </script>
@endpush