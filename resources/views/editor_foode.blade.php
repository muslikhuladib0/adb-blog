@extends('layouts.foode')

@section('title', 'post')

@section('head')
<link href='https://fonts.googleapis.com/css?family=Chelsea Market' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css">
{{-- <link rel="stylesheet" href=""> --}}


{{-- <link href="doka.min.css" rel="stylesheet"> --}}
@endsection

@section('content')

    @if ( old('categories') )
        @php
            old('categories')
        @endphp
    @endif

{{-- @if(session()->get('errors'))
{{ $errors->getBag('default')->first('thumbnail') }}
@endif --}}


{{-- @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            @php
                $errorMessage = ['ok'];
                array_push($errorMessage, $error);
            @endphp
        @endforeach
        {{ dd($errorMessage) }}
@endif --}}
{{-- @if (count($errors) > 0)
{{ dd($errors)}}
@endif --}}

    @if( $editor == 'new post')
        <form action="/editor" method="POST" style="margin: 50px;" enctype="multipart/form-data">
            @csrf

            <input type="text" name="title" class="form-control mb-2 @error('title') is-invalid @enderror"
                placeholder="title : blog terbaik yang saya temui adalah blog ini.." value="{{ old('title') }}">

            @error('title')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <textarea class="form-control @error('content') is-invalid @enderror" id="summary-ckeditor"
                name="content" >{{ old('content') }}</textarea><br>

            @error('content')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            
            <span style="position: relative; width: 100px; height: 100px;">
                <img id="blah" alt="" width="100" height="100" name="mantap"  value="{{ old('thumbnail') }}"/>
                <p
                    style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 70%; font-family: 'Chelsea Market'; text-align: center; background-color: white; width: 98%; opacity: 0.8;">
                    thumbnail</p>
                </span>
            </div> 

            <input type="file" class="@error('thumbnail') is-invalid @enderror"
                onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" name="thumbnail"
                id="files" hidden>

            {{-- <input type="file" id="files" hidden class=" @error('thumbnail1') is-invalid @enderror"/> --}}
            {{-- <input type="file" name="thumbnail1" id=""> --}}
            {{-- <input type="file" name="imageFile" id=""> --}}
            {{-- @if(session()->get('errors'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->getBag('default')->first('thumbnail') }}</strong>
                </span>    
            @endif --}}
            <br>
            @error('thumbnail')
                <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
            @enderror
                <br>
            <label for="files" class="sel-file" name="thumbnail">Select Thumbnail</label>
            
            <br>
            {{--  selain diheroku --}}
            <div class="form-group">
                <select class="select-2 is-invalid col-sm-10" id="ee9" multiple req style="width:100%;" name="categories[]">
                    @for ($i = 0; $i < count($allCategory); $i++)
                            <option value="{{ $allCategory[$i]['id'] }}">{{ $allCategory[$i]['category_name'] }}</option>
                    @endfor
                </select>
            </div>

            {{-- buat diheroku --}}
            {{-- <div class="form-group">
                <select class="select-2 is-invalid col-sm-10" id="ee9" multiple req style="width:100%;" name="categories[]">
                    @for ($i = 0; $i < count($allCategory); $i++)
                            <option value="{{ $allCategory[$i]['category_name'] }}">{{ $allCategory[$i]['category_name'] }}</option>
                    @endfor
                </select>
            </div> --}}

            <div class="form-group">
                <select class="select-2 is-invalid col-sm-10" id="e9" multiple req style="width:100%;" name="tags[]">
                </select>
            </div>

            {{-- <input type="file" name="gambar1" id="gambar1" hidden> --}}
            {{-- <input type="hidden" name="image" id="image"/> --}}

            {{-- <input id="upload-Image" type="file" onchange="loadImageFile();" name="gambar"/> --}}
            <br><br>
            <button type="submit" class="btn btn-primary mt-2">post</button>
        </form>

    @elseif ( $editor == 'edit post')
        <form action="/update post" method="POST" style="margin: 50px;" enctype="multipart/form-data">
            @csrf

            <input type="text" value="{{ $post['id'] }}" name="id" hidden>

            <input type="text" name="title" class="form-control mb-2 @error('title') is-invalid @enderror" value="{!! Request::old('title', $post->title) !!}">
            {{-- <input type="text" class="@error('title') is-invalid @enderror" name="title" value="{!! Request::old('title', $post->title) !!}" style="width: 100%;"> --}}
            
            @error('title')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <br>    
            
            <textarea class="form-control @error('content') is-invalid @enderror" id="summary-ckeditor"
                name="content">{!! Request::old('content', $post->content) !!}</textarea>

            @error('content')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

                <br>
            
            <div class="container-fluid" style="display:inline-block; ">
                <img src="https://res.cloudinary.com/duh6epdw5/image/upload/{{ $post['thumbnail'] }}" alt="thumbnail"
                    style="width: 10%; height: auto;">
                <span style="position: relative; width: 100px; height: 100px;">
                    <img id="blah" alt="" width="100" height="100" name="mantap" />
                    <p
                    style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 70%; font-family: 'Chelsea Market'; text-align: center;">
                    thumbnail baru</p>
                </span>
                <br> <br>
            </div>

            <input type="file" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])"
                name="thumbnail">
                <br>
            @error('thumbnail')
                <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
            @enderror
            <br>
            
            {{-- selain heroku --}}
            <div class="form-group">
                <select class="select-2 is-invalid col-sm-10" id="ee9" multiple req style="width:100%;" name="categories[]">
                    @for ($i = 0; $i < count($allCategory); $i++)
                        @if (count($post['categoriesName']) > 0)
                        @for ($k = 0; $k < count($post['categoriesName']); $k++)
                            @if ( $allCategory[$i]['category_name'] == $post['categoriesName'][$k]['category_name'] )
                                <option value="{{ $allCategory[$i]['id'] }}" selected>{{ $allCategory[$i]['category_name'] }}</option>
                            @else
                                <option value="{{ $allCategory[$i]['id'] }}">{{ $allCategory[$i]['category_name'] }}</option>
                            @endif
                        @endfor
                        @else
                        <option value="{{ $allCategory[$i]['id'] }}">{{ $allCategory[$i]['category_name'] }}</option>
                        @endif
                    @endfor
                </select>
              </div>

                {{-- buat diheroku --}}
                {{-- <div class="form-group">
                    <select class="select-2 is-invalid col-sm-10" id="ee9" multiple req style="width:100%;"
                        name="categories[]">
                        @for ($i = 0; $i < count($allCategory); $i++) @if (count($post['categoriesName'])> 0)
                            @for ($k = 0; $k < count($post['categoriesName']); $k++) 
                                @if ( $allCategory[$i]['category_name']==$post['categoriesName'][$k]['category_name'] ) 
                                    <option value="{{ $allCategory[$i]['category_name'] }}" selected>{{ $allCategory[$i]['category_name'] }}</option>
                                @else
                                    <option value="{{ $allCategory[$i]['category_name'] }}">{{ $allCategory[$i]['category_name'] }}</option>
                                @endif
                            @endfor
                                @else
                                <option value="{{ $allCategory[$i]['category_name'] }}">{{ $allCategory[$i]['category_name'] }}</option>
                                @endif
                        @endfor
                    </select>
                </div> --}}

            <div class="form-group">
                <select class="select-2 is-invalid col-sm-10" id="e9" multiple req style="width:100%;" name="tags[]">
                    @for ($i = 0; $i < count($tags); $i++)
                        <option value="{{ $tags[$i] }}" selected>{{ $tags[$i] }} </option>
                    @endfor
                </select>
              </div>

            <button type="submit" class="btn btn-primary mt-2">update</button>
            
        </form>

    @endif


    <br><br><br>
    <div class="container">
        
    </div>




<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js"></script>

<script>
    CKEDITOR.replace('summary-ckeditor', {
        filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });

  


    $(() => {
        
    });

</script>
<script>
    $("#e9").select2({
        tags: true,
        placeholder: "masukan tag",
    });
    $("#ee9").select2({
        placeholder: "pilih kategori"
    });

    // $('.select2-selection__choice').css({"padding": "6px",
    // "background": "white"});
    // $(document).ready(function() { $("#e9").select2(); });
</script>

<script type="text/javascript">
    $( function() {
        var fileReader = new FileReader();
    var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
    
    fileReader.onload = function (event) {
      var image = new Image();
      
      image.onload=function(){
        //   document.getElementById("original-Img").src=image.src;
          var canvas=document.createElement("canvas");
          var context=canvas.getContext("2d");
          canvas.width=image.width/4;
          canvas.height=image.height/4;
          context.drawImage(image,
              0,
              0,
              image.width,
              image.height,
              0,
              0,
              canvas.width,
              canvas.height
          );
          
          document.getElementById("upload-Preview").src = canvas.toDataURL();
        
      }
      image.src=event.target.result;
    };
    
    var loadImageFile = function () {
      var uploadImage = document.getElementById("gambar");
        // console.log(uploadImage);
      
      //check and retuns the length of uploded file.
      if (uploadImage.files.length === 0) { 
        return; 
      }
      
      //Is Used for validate a valid file.
      var uploadFile = document.getElementById("").files[0];
      if (!filterType.test(uploadFile.type)) {
        alert("Please select a valid image."); 
        return;
      }
      
      fileReader.readAsDataURL(uploadFile);
      $('#gambar').val(uploadFile);
    //   document.getElementById('gambar1').src = uploadFile;
    }

    });
    </script>
@endsection
