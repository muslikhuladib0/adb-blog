<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'teknologi', 'sepakbola', 'hiburan', 'makanan|minuman', 'tutorial'
        ];

        for ($i=0; $i < count($categories); $i++) { 
            DB::table('categories')->insert([
                'category_name' => $categories[$i]
            ]);
        }
    }
}
