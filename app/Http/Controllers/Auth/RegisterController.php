<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // dd($data);
        // request()->validate([
        //     'username' => ['required', 'min:5', 'string', 'unique:users'],
        //     'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        //     'password' => [ 'required', 'string', 'min:6', 'confirmed']
        // ],
        // [
        //     'username.required' => 'username belum di isi!!!',
        //     'username.min' => 'username belum 5 huruf atau lebih',
        //     'username.string' => 'username bukan string',
        //     'username.unique' => 'username sudah ada pakai username lain',
        //     'email.required' => 'email belum di isi!!!',
        //     'email.string' => 'emailnya bukan string',
        //     'email.email' => 'bukan email yang di isikan',
        //     'email.max' => 'email max 255 huruf, coba hapus beberapa',
        //     'email.unique' => 'email udah ada silahkan login',
        //     'password:confirmed' => 'password tidak sama samakan yang diatas',
        //     'password:required' => 'password belum di isi!!!',
        //     'password:string' => 'password bukan string',
        //     'password:min' => 'password minimal 6 huruf'
        // ]);
        
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ],
        [
            'username.required' => 'username belum di isi!!!',
            'username.min' => 'username belum 5 huruf atau lebih',
            'username.string' => 'username bukan string',
            'username.unique' => 'username sudah ada pakai username lain',
            'email.required' => 'email belum di isi!!!',
            'email.string' => 'emailnya bukan string',
            'email.email' => 'bukan email yang di isikan',
            'email.max' => 'email max 255 huruf, coba hapus beberapa',
            'email.unique' => 'email udah ada silahkan login',
            'password:confirmed' => 'password tidak sama samakan yang diatas',
            'password:required' => 'password belum di isi!!!',
            'password:string' => 'password bukan string',
            'password:min' => 'password minimal 6 huruf'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd('helo');
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    public function showRegistrationForm()
    {
        return redirect('/user/register');
    }
}
