<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginRegisterController extends Controller
{
    public function index($lr) 
    {
        if($lr == 'login') {
            $loginOrRegister = 'login';
            return view('/auth/login', compact('loginOrRegister'));
        } else {
            $loginOrRegister = 'register';
            return view('/auth/login', compact('loginOrRegister'));
        }
    }
}
