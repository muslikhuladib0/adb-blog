<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_Post extends Model
{
    protected $table = 'categories_post';

    protected $fillable = ['post_id', 'category_id'];

    function categories() {
        return $this->hasMany(Category::class, 'id');
    }

    
}
 